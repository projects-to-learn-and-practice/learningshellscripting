1- Given the following file tasks.txt, use the cut command to extract columns 1 (Image Name), 2 (PID), and 5 (Mem Usage).
    Image Name;PID;Session Name;Session#;Mem Usage
    System Idle Process;0;Services;0;4 K
    System;4;Services;0;2,140 K
    smss.exe;340;Services;0;1,060 K
    csrss.exe;528;Services;0;4,756 K
2- Given the file procowner.txt, use the join command to merge the file with tasks.txt from the preceding exercise.
    Process Owner;PID
    jdoe;0
    tjones;4
    jsmith;340
    msmith;528
3- Use the tr command to replace all of the semicolon characters in tasks.txt with the tab character and print the file to the screen.
4- Write a command that extracts the first and last names of all authors in book.json.
