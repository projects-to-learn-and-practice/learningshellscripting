#!/usr/local/bin/bash

printf "%s\n" "Commands used in book to explain the concepts"
echo
echo "---------------------------------"
printf "%s\n" "AWK"
printf "%s" "Content of awkusers.txt:"
printf "\n%s" "$(awk '{print $0}' ./awkusers.txt)"
echo
echo
printf "%s" "Output of: awk '\$2 == "Jones" {print \$0}' ./awkusers.txt"
printf "\n%s" "$(awk '$2 == "Jones" {print $0}' ./awkusers.txt)"
echo
echo
printf "%s" "Output of: awk -F " " '{print \$2}' ./awkusers.txt"
printf "\n%s" "$(awk -F " " '{print $2}' ./awkusers.txt)"
echo
echo "---------------------------------"
printf "%s\n" "JOIN"
printf "%s" "Content of accesstime.txt:"
printf "\n%s" "$(awk ' {print $0}' ./accesstime.txt)"
echo
echo
printf "%s" "Content of usernames.txt:"
printf "\n%s" "$(awk ' {print $0}' ./usernames.txt)"
echo
echo
printf "%s" "Output of: join -1 3 -2 1 -t, ./accesstime.txt ./usernames.txt"
printf "\n%s" "$(join -1 3 -2 1 -t, ./accesstime.txt ./usernames.txt)"
echo
echo "---------------------------------"
printf "%s\n" "SED"
printf "%s" "Content of ips.txt:"
printf "\n%s" "$(awk ' {print $0}' ./ips.txt)"
echo
echo
printf "%s" "Output of: sed 's/10\.0\.4\.35/10.0.4.27/g' ./ips.txt"
printf "\n%s" "$(sed 's/10\.0\.4\.35/10.0.4.27/g' ./ips.txt)"
echo
echo "---------------------------------"
printf "%s\n" "tail"
printf "%s" "Content of ips.txt:"
printf "\n%s" "$(awk ' {print $0}' ./ips.txt)"
echo
echo
printf "%s" "Output of: tail -n 1 ./ips.txt"
printf "\n%s" "$(tail -n 1 ./ips.txt)"
echo
echo "---------------------------------"
printf "%s\n" "tr"
printf "%s" "Content of infile.txt:"
printf "\n%s" "$(awk ' {print $0}' ./infile.txt)"
echo
echo
printf "%s" "Content of outfile.txt:"
printf "\n%s" "$(awk ' {print $0}' ./outfile.txt)"
echo
echo
printf "%s" "Command: tr '\\:'  '/|' < ./infile.txt  > ./outfile.txt"
tr '\\:'  '/|' < ./infile.txt  > ./outfile.txt
echo
echo
printf "%s" "Content of outfile.txt after Command:"
printf "\n%s" "$(awk ' {print $0}' ./outfile.txt)"
echo
echo "---------------------------------"
printf "%s\n" "Processing delimited files"
printf "%s" "Content of csvex.txt:"
printf "\n%s" "$(awk ' {print $0}' ./csvex.txt)"
echo
echo
printf "%s" "Command: cut -d',' -f1 ./csvex.txt"
printf "\n%s" "$(cut -d',' -f1 ./csvex.txt)"
echo
echo
printf "%s" "Command: cut -d',' -f1-3 ./csvex.txt | tr -d '\"'"
printf "\n%s" "$(cut -d',' -f1-3 ./csvex.txt | tr -d '"')"
echo
echo
printf "%s" "Command: cut -d',' -f1,4 ./csvex.txt | tr -d '\"' | tail -n +2"
printf "\n%s" "$(cut -d',' -f1,4 ./csvex.txt | tr -d '"' | tail -n +2)"
echo
echo "---------------------------------"
printf "%s\n" "Iterating through delimited files"
printf "%s" "Content of csvex.txt:"
printf "\n%s" "$(awk ' {print $0}' ./csvex.txt)"
echo
echo
printf "%s" "Content of passwords.txt:"
printf "\n%s" "$(awk ' {print $0}' ./passwords.txt)"
echo
echo
printf "%s" "Command: grep \"\$(awk -F \",\" '{print \$4}' ./csvex.txt)\" ./passwords.txt"
printf "\n%s" "$(grep "$(awk -F "," '{print $4}' ./csvex.txt)" ./passwords.txt)"
echo
echo "---------------------------------"
printf "%s\n" "Processing XMLs"
printf "%s" "Content of book.xml:"
printf "\n%s" "$(awk ' {print $0}' ./book.xml)"
echo
echo
printf "%s" "Command: grep -o '<firstName>.*<\/firstName>' book.xml"
printf "\n%s" "$(grep -o '<firstName>.*<\/firstName>' book.xml)"
echo
echo
printf "%s" "Command: grep -o '<firstName>.*<\/firstName>' book.xml | sed 's/<[^>]*>//g'"
printf "\n%s" "$(grep -o '<firstName>.*<\/firstName>' book.xml | sed 's/<[^>]*>//g' )"
echo
echo
printf "%s\n" "Command for multiline search: grep -Pzo '(?s)<author>.*?<\/author>' ./book.xml"
printf "\n%s" "$(grep -Pzo '(?s)<author>.*?<\/author>' ./book.xml)"
echo
echo "---------------------------------"
printf "%s\n" "Processing JSON"
printf "%s" "Content of book.xml:"
printf "\n%s" "$(awk ' {print $0}' ./book.json)"
echo
echo
printf "%s" "Command: grep -o '"firstName": ".*"' book.json"
printf "\n%s" "$(grep -o '"firstName": ".*"' book.json)"
echo
echo "---------------------------------"
printf "%s\n" "Processing JSON with jq"
printf "%s" "Command: jq '.title' book.json"
printf "\n%s" "$(jq '.title' book.json)"
echo
echo
printf "%s" "Command: jq '.authors[].firstName' book.json"
printf "\n%s" "$(jq '.authors[].firstName' book.json)"
echo
echo "---------------------------------"
