#!/bin/bash

# PROBLEM STATEMENT:
# Write a regular expression that
# will match if the hexadecimal string 0x90 occurs more than three times in a row (i.e., 0x90 0x90 0x90).

regex="(0x90[[:space:]]*){3,}"

read -p "Enter an hex:" input

echo $input
echo $regex

if [[ $input =~ $regex ]]
then
  echo "Match"
else
  echo "No Match"
fi
