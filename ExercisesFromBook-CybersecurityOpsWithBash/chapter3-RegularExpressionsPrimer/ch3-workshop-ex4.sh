#!/bin/bash

# PROBLEM STATEMENT:
# Write a regular expression that
# uses grouping to match on the following two IP addresses: 10.0.0.25 and 10.0.0.134.

regex="(10\.0\.0\.25|10\.0\.0\.134)"

echo "Enter an IP to check a match on the following two IP addresses: 10.0.0.25 and 10.0.0.134"

read -p "Enter an IP:" input

if [[ $input =~ $regex ]]
then
  echo "Match"
else
  echo "No Match"
fi
