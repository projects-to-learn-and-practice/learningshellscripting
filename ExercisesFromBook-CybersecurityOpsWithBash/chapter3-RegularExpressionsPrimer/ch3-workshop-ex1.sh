#!/bin/bash

# PROBLEM STATEMENT:
# Write a regular expression that matches a floating-point number (a number with a decimal point) such as 3.14.
# There can be digits on either side of the decimal point, but there need not be any on one side or the other.
# Allow the regex to match just a decimal point by itself, too.

floatregex="[0-9]*\.[0-9]*"

read -p "Enter a float:" fl

echo "Checking if a float was entered"

if [[ $fl =~ $floatregex ]]
then
  echo "$fl is a float"
else
  echo "$fl is not a float"
fi
