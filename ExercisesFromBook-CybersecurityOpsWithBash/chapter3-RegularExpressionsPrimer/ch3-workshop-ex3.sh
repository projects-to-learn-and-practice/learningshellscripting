#!/bin/bash

# PROBLEM STATEMENT:
# Write a regular expression
# that looks for a line that begins with a digit and ends with a digit, with anything occurring in between.

regex="^[0-9].*[0-9]\$"

echo "Enter input and program will check that a line begins with a digit and ends with a digit, with anything occurring in between"
read -p "Enter a line to check:" line

if [[ $line =~ $regex ]]
then
  echo "Match"
else
  echo "No Match"
fi
