#!/bin/bash

# PROBLEM STATEMENT:
# Use a back reference in a regular expression to match a number that appears on both sides of an equals sign.
# For example, it should match “314 is = to 314” but not “6 = 7.”

regex="([0-9]+).*=.*\1"

#\1 is back reference to ([0-9]+)

echo "User need to enter some text with numbers and then = sign and again some text with some number"
echo "Programe then, should for example match “314 is = to 314” but not “6 = 7.”"
echo
read -p "Enter the input in double quotes: " input

if [[ $input =~ $regex ]]
then
  echo "Match"
else
  echo "No Match"
fi



# Solution provided by book aother itself matches my solution.
# The problem statement and solution are not quite correct and clear and gives inconsistent results.
# “314 is = to 314” is Match
# “314 is = to 312” is Match
# “314 = to 314” is Match
# But “2 = 2” is not a match

# “6 = 7.” is Not a match
# But “16 = 17” is a Match
