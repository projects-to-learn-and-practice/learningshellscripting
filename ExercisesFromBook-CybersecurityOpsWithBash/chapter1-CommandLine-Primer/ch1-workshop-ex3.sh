#!/bin/bash

# PROBLEM STATEMENT:
# Write a command that copies all of the files in the directory /etc/a to the directory /etc/b
# and redirects standard error to the file copyerror.log.

# ------------------------------------------------------- #
# SETTING UP OUTPUT DIRECTORY
OutputDir=../AllExerciseOutputs/ch1

if [ ! -d $OutputDir ]
then
  mkdir ../AllExerciseOutputs/ch1
fi

# ------------------------------------------------------- #
# MAIN PROGRAM
cp /etc/a /etc/b 2> $OutputDir/copyerror.log
