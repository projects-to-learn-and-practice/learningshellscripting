#!/bin/bash

# PROBLEM STATEMENT:
# Write a command that performs a directory listing (ls) on the root file directory
# and pipes the output into the more command.

# ------------------------------------------------------- #
# MAIN PROGRAM
ls | more
