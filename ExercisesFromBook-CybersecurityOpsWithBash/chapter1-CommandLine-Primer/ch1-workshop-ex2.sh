#!/bin/bash

# PROBLEM STATEMENT:
# Write a command that executes ifconfig
# and redirects standard output
# and appends it to a file named ipaddress.txt

# ------------------------------------------------------- #
# SETTING UP OUTPUT DIRECTORY
OutputDir=../AllExerciseOutputs/ch1

if [ ! -d $OutputDir ]
then
  mkdir ../AllExerciseOutputs/ch1
fi

# ------------------------------------------------------- #
# MAIN PROGRAM
ifconfig >> $OutputDir/ipaddress.txt
