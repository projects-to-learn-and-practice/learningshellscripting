#!/bin/bash

# PROBLEM STATEMENT:
# Modify hashsearch.sh to have an option (-1) to quit after finding a match.
# If the option is not specified, it will keep searching for additional matches.


HASH=$1
DIR=${2:-.}	# default is here, cwd

# convert pathname into an absolute path
function mkabspath ()				#6
{
    if [[ $1 == /* ]]				#7
    then
    	ABS=$1
    else
    	ABS="$PWD/$1"				#8
    fi
}

find $DIR -type f |				#1
while read fn
do
    THISONE=$(sha1sum "$fn")			#2
    THISONE=${THISONE%% *}			#3
    if [[ $THISONE == $HASH ]]
    then
      mkabspath "$fn"				#4
    	echo $ABS				#5
      echo "Enter -1 to quit or something else to continue search: "
      read -n 2 choice<&1
      if [[ $choice == '-1' ]]
      then
        printf "\nSearch stopped \n"
        break
      fi
    fi
done
