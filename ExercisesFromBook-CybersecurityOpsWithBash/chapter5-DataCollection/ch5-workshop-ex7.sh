#!/bin/bash

# PROBLEM STATEMENT:


# Modify hashsearch.sh to simplify the full pathname that it prints out:
  # 1- If the string it outputs is /home/usr07/subdir/./misc/x.data,
  #    modify it to remove the redundant ./ before printing it out.
  # 2- If the string is /home/usr/07/subdir/../misc/x.data,
  #    modify it to remove the ../ and also the subdir/ before printing it out.

  HASH=$1
  DIR=${2:-.}	# default is here, cwd

  # convert pathname into an absolute path
  function mkabspath ()				#6
  {
      if [[ $1 == /* ]]				#7
      then
      	ABS=$1
      else
        echo $1
        EXTRACTEDFN=$(echo "$1" | rev | cut -d / -f 1 | rev)
      	ABS="$PWD/$EXTRACTEDFN"				#8
      fi
  }

  find $DIR -type f |				#1
  while read fn
  do
      THISONE=$(sha1sum "$fn")			#2
      THISONE=${THISONE%% *}			#3
      if [[ $THISONE == $HASH ]]
      then
        mkabspath "$fn"				#4
      	echo $ABS				#5
        echo "Enter -1 to quit or something else to continue search: "
        read -n 2 choice<&1
        if [[ $choice == '-1' ]]
        then
          printf "\nSearch stopped \n"
          break
        fi
      fi
  done
