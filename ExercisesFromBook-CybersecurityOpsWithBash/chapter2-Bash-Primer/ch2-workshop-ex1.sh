#!/bin/bash

# PROBLEM STATEMENT:
# Experiment with the uname command, seeing what it prints on the various operating systems.
# Rewrite the osdetect.sh script to use the uname command, possibly with one of its options.
# Caution: not all options are available on every operating system.

# uname [-amnprsv]

if [[ $(uname -s) -eq "Darwin" ]]; then
  echo "MacOS"
elif [[ $(uname -s) -eq "Linux" ]]; then
  echo "Linux"
else
  echo "Not MacOS and Linux. Some other OS"
fi
