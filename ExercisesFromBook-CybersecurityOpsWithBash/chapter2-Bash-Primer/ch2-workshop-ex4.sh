#!/bin/bash

# PROBLEM STATEMENT:
# Write a script that tells how many arguments are supplied to the script.

#    1. Modify your script to have it also echo each argument, one per line.
#    2. Modify your script further to label each argument like this:
#        $ bash ch2-workshop-ex4.sh this is a "real live" test
#
#        there are 5 arguments
#        arg1: this
#        arg2: is
#        arg3: a
#        arg4: real live
#        arg5: test
#        $

echo "there are $# arguments"
echo -en "\n"

i=1

for ARG in "$@"
do
  echo "arg$i: "$ARG
  let i=$i+1
done
