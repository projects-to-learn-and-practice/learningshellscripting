#!/bin/bash

# PROBLEM STATEMENT:
# Set the permissions on the osdetect.sh script to be executable (see man chmod)
# so that you can run the script without using bash as the first word on the command line.
# How do you now invoke the script?

# chmod +x 
