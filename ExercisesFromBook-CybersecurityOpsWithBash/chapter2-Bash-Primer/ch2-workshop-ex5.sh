#!/bin/bash

# PROBLEM STATEMENT:
# Modify ch2-workshop-ex4.sh so it lists only the even arguments.

echo "there are $# arguments"
echo -en "\n"

i=1

for ARG in "$@"
do
  if [[ $(($i % 2)) -eq 0 ]]
  then
    echo "arg$i: "$ARG
  fi
  let i=$i+1
done
