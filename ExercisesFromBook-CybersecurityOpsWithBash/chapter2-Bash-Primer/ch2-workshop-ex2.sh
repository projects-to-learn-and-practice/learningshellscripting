#!/bin/bash

# PROBLEM STATEMENT:
# Modify the osdetect.sh script to use a function.
# Put the if/then/else logic inside the function and then call it from the script.
# Don’t have the function itself produce any output.
# Make the output come from the main part of the script.

#FUNCTIONS

OS=unset

checkOS() {
  if type -t wevtutil &> /dev/null
  then
      OS=MSWin
  elif type -t scutil &> /dev/null
  then
      OS=macOS
  else
      OS=Linux
  fi
}

# MAIN PROGRAM

checkOS
echo $OS
