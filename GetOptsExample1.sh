#!/bin/bash

# A very simple script to accept options l, h, a and an arguement with option 'a'

#Below options are not mandatory but good habit to use in all scripts
set -e  # short for errexit
set -u  # makes the shell to treat undefined variables as errors
set -o pipefail # changes the way commands inside a pipe are evaluated.
                # The exit status of a pipe will be that of the rightmost command to exit with a non zero status,
                # or zero if the all the programs in the pipe are executed successfully

while getopts 'lha:' OPTION; do  # Options "l, h, a". ':' means that the option requires an argument.
  case "$OPTION" in  # Each parsed option will be stored inside the $OPTION variable
    l)
      echo "linuxconfig"
      ;;
    h)
      echo "h stands for h"
      ;;
    a)
      avalue="$OPTARG"  # An argument, when present, will become the value of the $OPTARG
      echo "The value provided is $OPTARG"
      ;;
    ?)
      echo "script usage: $(basename $0) [-l] [-h] [-a somevalue]" >&2
      exit 1
      ;;
  esac
done
shift "$(($OPTIND -1))"

# shift is a shell builtin which moves the positional parameters of the script down
# a specified number of positions provided to it as a positive number, discarding the related arguments.
# $OPTIND variable is set to 1, and it is incremented each time an option is parsed, until it reaches the last one.
# if we make a shift of the value of $OPTIND - 1 (that is the effective number of the parsed options) on the positional parameters,
# what remains are just the arguments that are not options
