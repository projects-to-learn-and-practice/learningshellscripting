#!/bin/bash

echo "type command to identify whether a word is a keyword, a built-in, a command, or none of those "
echo "type -t if -->"$(type -t if)
echo "type -t pwd -->"$(type -t pwd)
echo "type -t ls -->"$(type -t ls)
echo

echo "compgen command to determine what commands, built-ins, and keywords are available to you. Use the -c option to list commands, -b for built-ins, and -k for keywords"
echo "Example: compgen -k "
compgen -k
