#!/bin/bash

# Tip: Look At GetOptsExample1.sh for some of the comments explaining the script.

# SET SCRIPT OPTIONS.
set -e
set -u
set -o pipefail
unset Name

# DEFINE FUNCTIONS

Help()
{
  # Display Help
  echo "Syntax: $0 [-h|n]"
  echo "options:"
  echo "-h    Print this help."
  echo "-n    Enter Name."
  echo
}

# MAIN PROGRAM LOGIC

while getopts "hn:" OPTION;
do
  case "$OPTION" in
    h) #display Help
      Help
      exit
      ;;
    n)
      Name=$OPTARG
      ;;
    ?)
      echo "Syntax: $0 [-h|-n]"
      exit
      ;;
  esac
done

shift "$(($OPTIND -1))"
echo "Hello $Name"
