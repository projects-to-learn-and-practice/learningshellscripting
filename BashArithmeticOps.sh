#!/bin/bash

#This program would work for Integer input only.
#Arithmetic operations on Floats and Doubles would result in error.

#Get input from user
read -p "Enter Integer Number1: " num1
read -p "Enter Integer Number2: " num2

echo -en "\n" #Adds blank line in output
echo "num1=$num1"
echo "num2=$num2"

echo -en "\n" #Adds blank line in output

echo "***** 1 - USING expr COMMAND FOR CALCULATIONS *****"

echo "Sum=\`expr \${num1} + \${num2}\`"
echo "Sum is: `expr ${num1} + ${num2}`"

echo "Substraction=\`expr \${num1} - \${num2}\`"
echo "Substraction is: `expr ${num1} - ${num2}`"

echo "Mutiplication=\`expr \${num1} \\* \${num2}\`"
echo "NOTE: * is escaped while doing multiplication"
echo "Mutiplication is: `expr ${num1} \* ${num2}`"

echo "Division=\`expr \${num1} / \${num2}\`"
echo "Division is: `expr ${num1} / ${num2}`"

echo "Remainder=\`expr \${num1} % \${num2}\`"
echo "Remainder is:" `expr ${num1} % ${num2}`

echo -en "\n"
echo "USING expr COMMAND WITHIN COMMAND SUBSTITUTE"
echo "Example"
echo "Division=\$(expr \${num1} / \${num2})"
echo "Division is $(expr ${num1} / ${num2})"


echo -en "\n"
echo "***** 2- USING let COMMAND FOR CALCULATIONS *****"
echo "let sum=\${num1}+\${num2}"
let sum=${num1}+${num2}
echo "Sum is: "${sum}
echo "let sub=\${num1}-\${num2}"
let sub=${num1}-${num2}
echo "Substraction is: "${sub}
echo "let prod=\${num1}\*\${num2}"
let prod=${num1}\*${num2}
echo "Product is: "${prod}
echo "let div=\${num1}/\${num2}"
let div=${num1}/${num2}
echo "Division is: "${div}
echo "let rem=\${num1}%\${num2}"
let rem=${num1}%${num2}
echo "Remainder is: "${rem}

echo -en "\n"
echo "***** 3 - USING DOUBLE BRACKETS FOR CALCULATIONS *****"
echo "sum=\$((\${num1}+\${num2}))"
sum=$((${num1}+${num2}))
echo "Sum is: $sum"
echo "Another method: ((sum2=\${num1}+\${num2}))"
((sum2=${num1}+${num2}))
echo "Sum is: $sum2"
echo "Shorthand method: ((sum2+=10))"
((sum2+=10))
echo "Sum is: $sum2"
echo "POST and PRE INCREMENT/DECREMENT EXAMPLE"
echo "INCREMENT \$((++sum))"
echo "INCREMENT=$((++sum))"
echo "DECREMENT \$((--sum))"
echo "DECREMENT=$((--sum))"

echo -en "\n"
echo "***** 4 - USING bc COMMAND FOR FLOAT or DOUBLE NUMBERS *****"
echo "bc method produce results for operation on Float and Double input numbers as well"
echo "Division is: div=\$(echo "\"\${num1}/\${num2}\"" | bc)"
div=$(echo "${num1}/${num2}" | bc)
echo "Division result ${div}"
echo "Division is: div2=\$(echo "\"\${num1}/\${num2}\"" | bc -l)"
div2=$(echo "${num1}/${num2}" | bc -l)
echo "Division result in decimal points ${div2}"
echo "Division is: div2=\$(echo "\"scale=2\; \${num1}/\${num2}\"" | bc -l)"
div2=$(echo "scale=2; ${num1}/${num2}" | bc -l)
echo "Division result upto 2 decimal points ${div2}"

echo -en "\n"
echo "***** 5 - USING printf COMMAND FOR FLOAT or DOUBLE NUMBERS *****"
echo "Division method is: div=\$(printf \"%.2f\n\" "\"\$\(\(10**2 \* \$num1/\$num2\)\)e-2\"")"
div=$(printf "%.2f\n" "$((10**2 * $num1/$num2))e-2")
echo "Division result in 2 decimal points "$div


echo -en "\n"
echo "***** 6 - USING awk COMMAND FOR ARITHMETIC OPERATIONS *****"
echo "Division method is: divResult=\$(awk \""BEGIN {print \$num1/\$num2}\"")"
divResult=$(awk "BEGIN {print $num1/$num2}")
echo "Division result: "$divResult

echo -en "\n"
echo "***** 7 - CALCULATE PERCENTAGE OF A VALUE *****"
echo "Lets Calculate num2 as percentage of num1"
echo "Division method is: percent=\$(printf \"%.2f%%\n\" "\"\$\(\(10**3 \* 100 \* \$num2/\$num1\)\)e-3\"")"
percent=$(printf "%.2f%%\n" "$((10**3 * 100 * $num2/$num1))e-3")
echo "num2 is "$percent "of num1"
