#!/bin/bash

echo -en "\n"
echo "\$0 : Special Variable \$0 displays name of Bash Script."
echo "Output: "$0

echo -en "\n"
echo "\$# : Number of arguments passed to the script."
echo "Output: "$#

echo -en "\n"
echo "\$1 - \$9 : The first 9 arguments to the Bash script."
echo "Output of \$1: "$1

echo -en "\n"
echo "\$@ : All the arguments supplied to the Bash script."
echo "Output of \$@: "$@

echo -en "\n"
echo "\$? : The exit status of the most recently run process."
echo "Output of \$?: "$?

echo -en "\n"
echo "\$\$ : The process ID of the current script."
echo "Output of \$\$: "$$

echo -en "\n"
echo "\$USER : The username of the user running the script."
echo "Output of \$USER: "$USER

echo -en "\n"
echo "\$HOSTNAME : The hostname of the machine the script is running on."
echo "Output of \$HOSTNAME: "$HOSTNAME

echo -en "\n"
echo "\$SECONDS : The number of seconds since the script was started."
echo "Output of \$SECONDS: "$SECONDS

echo -en "\n"
echo "\$RANDOM : Returns a different random number each time is it referred to."
echo "Output of \$RANDOM: "$RANDOM

echo -en "\n"
echo "\$LINENO : Returns the current line number in the Bash script."
echo "Output of \$LINENO: "$LINENO

echo -en "\n"
echo "\$(env) : Returns the list of all current envitonment variables with values."
echo "Output of \$(env): "
echo "$(env)" | tr " " "\n"
